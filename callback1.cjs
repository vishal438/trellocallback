const fs = require("fs");
const path = require("path");

//path to "boards.json"
const boardData = path.resolve("boards.json");

//function takes board id and callback as input
function problem1(boardId, callback) {
  setTimeout(() => {

    fs.readFile(boardData, "utf-8", (err, data) => {
      
      if (err) {
        console.log(err);
      } else {
        const boardData = JSON.parse(data);

        boardData.map((item) => {
          // if the id matches callback function is invoked.
          if (item.id === boardId) {
            return callback(null, [item]);
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = problem1;

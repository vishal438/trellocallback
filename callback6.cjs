const fs = require('fs');
const path = require('path');

const callbackFunction1 = require('./callback1.cjs');
const callbackFunction2 = require('./callback2.cjs');
const boardData1 = require('./boards.json');


const callbackFunction3 = require('./callback3.cjs');
const listData = require('./lists.json');




function problem6(id) {


    callbackFunction1(id, (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(data);
            callbackFunction2(id, (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(data);

                    
                    const cardMind = data.map((item) => {
                        return item['id']
                    })
                   

                    cardMind.map((id) => {

                        callbackFunction3(id, (err, data) => {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log(data);
                            }
                        });
                    });
                }
            })
        }

    });

}
module.exports = problem6;

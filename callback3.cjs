const fs = require("fs");
const path = require("path");

//path to "cards.json"
const cardsData = path.resolve("cards.json");

function problem3(listId, callback) {

  setTimeout(() => {
     //reading cards data
    fs.readFile(cardsData, "utf-8", (err, data) => {

      if (err) {
        console.log(err);

      } else {
        // parsing cards data
        const cardData = JSON.parse(data);

        return callback(null, cardData[listId]);
      }
      
    });
  }, 2 * 1000);
};
module.exports = problem3;

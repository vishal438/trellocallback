const fs = require("fs");
const path = require("path");

//path to "list.json"
const listData = path.resolve("lists.json");

//function takes boarid and callback as argument
function problem2(boardId, callback) {

  setTimeout(() => {
    fs.readFile(listData, "utf-8", (err, data) => {
      if (err) {
        
        console.log(err);
      } else {

        const boardData = JSON.parse(data);
        
        //get the key id of boards data
        const listDataKeys = Object.keys(boardData);
        // iterating through the array to find the item that matches the boardid
        listDataKeys.map((item) => {
          if (item === boardId) {
            return callback(null, boardData[item]);
            
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = problem2;

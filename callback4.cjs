
const fs = require('fs');
const path = require('path');

const callbackFunction1 = require('./callback1.cjs');
const callbackFunction2 = require('./callback2.cjs');
const boardData1 = require('./boards.json');


const callbackFunction3 = require('./callback3.cjs');
const listData = require('./lists.json');




function problem4(id) {


    callbackFunction1(id, (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(data);
            callbackFunction2(id, (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(data);

                    const cardMind = data.find((item) => {
                        return item.name === "Mind"
                    })
                    const id = cardMind.id;


                    function3(id);
                }
            })
        }

    });
    function function3(id) {
        
        callbackFunction3(id, (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log(data);
            }
        });
    }

}
module.exports = problem4;

const callbackFunction3 = require("../callback3.cjs");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
}

callbackFunction3("qwsa221", callback);

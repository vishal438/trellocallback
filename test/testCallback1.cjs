const callbackFunction1 = require("../callback1.cjs");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
}

callbackFunction1("mcu453ed", callback);

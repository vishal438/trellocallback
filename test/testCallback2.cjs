const callbackFunction2 = require("../callback2.cjs");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
}

callbackFunction2("mcu453ed", callback);
